import numpy as np


def vanilla(dE, dW, dB, X, params):
        """vanilla learning - stochastic gradient descent
        
        :parameters:
                dE      : error gradient
                dW      : weight gradient
                dB      : bias gradient
                X       : inputs of the layer
                params  : [learningrate]

        :return:
                dW : delta weights
                dB : delta bias
        """
        dW = params * X.T @ dE
        dB = params * np.sum(dE, 0)
        return dW, dB

def momentum(dE, dW, dB, X, params):
        """momentum learning - stochastic gradient descent with momentum
        
        :parameters:
                dE      : error gradient
                dW      : weight gradient
                dB      : bias gradient
                X       : inputs of the layer
                params  : [learningrate, momentum]
        
        :return:
                dW : delta weights
                dB : delta bias
        """
        dW = params[0] * dW + params[1] * (X.T @ dE)
        dB = params[0] * dB + params[1] * np.sum(dE, 0)
        return dW, dB
