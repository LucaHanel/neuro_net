import numpy as np

def read_dataset(path_data, sample_separator = ':', data_separator = ' ', 
                        trailing_newline = True, set_separator = None):
    """ Read data set from file.
        
    By default, it is assumed that one sample is in each line, target and data are
    separated by ':', data is separated by ' '.

    However, all separators can be changed, including that samples may all be written
    in one line. Then a set separator is also needed.

    :paramters: path_data: path to file containing the data (both input and target)
                sapmple_separator: char, separator used to separate data from target
                data_separator: char, separator used to separate the inputs
                trailing_newline: bool, true if samples are separated by newline
                set_separator: char, separator used to separate samples (only needed
                               if samples are not separated by newline)
    :return:
    """
    dataF = open(path_data)
    data = []
    labels = []
    if trailing_newline:
        for line in dataF:
            line = line[:-1]
            dataStr, label = line.split(sample_separator)
            dat = []
            for d in dataStr.split(data_separator):
                dat.append(float(d))
            data.append(dat)
            lab = int(label)
            labels.append(lab)
        data = np.array(data)
        labels = np.array(labels)
        return data, labels
    else:
        for sample in dataF.split(set_separator):
            dataStr, label = sample.split(sample_separator)
            dat = []
            for d in dataStr.split(data_separator):
                dat.append(float(d))
            data.append(dat)
            lab = int(label)
            labels.append(lab)
        data = np.array(data)
        labels = np.array(labels)
        return data, labels

def normalize_data(data, mean = None, variance = None):
    """ A function to normalize the mean and variance of the data set

        One can optionally pass mean and variance for normalizing
        the data with specific parameters

    :parameters: data : the data set to normalize
                 mean : mean to be used for normalization
                 variance: variance to be used for normalization
    :return: normalized data, normalization parameters
    """
    if mean == None:
        mean = 0
        for sample in data:
            mean += np.mean(sample)
        mean /= len(data)

    if variance == None:
        variance = 0
        for sample in data:
            for subsample in sample:
                variance += ((subsample - mean)**2) /  len(sample)
        variance /= len(data)

    data = (data - mean) / variance
    return data, mean, variance