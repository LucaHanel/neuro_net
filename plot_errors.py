import numpy as np
import nn_modules
import nn_net
import matplotlib.pyplot as plt
import os

path = 'savedNets/'
nets = sorted(os.listdir(path))


errors = []
for n in nets:
    net = open(path + n)
    for line in net:
        line = line[:-1]
        if line[0] == 'l':
            err = [float(x) for x in line[1:].split(',')]
            errors.append(err)

for err in errors:
    plt.plot(err)

plt.ylabel('Loss')
plt.xlabel('epoch')
plt.legend(nets)
plt.show()