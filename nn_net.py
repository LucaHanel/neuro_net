import numpy as np
import nn_modules
import learning_functions

class nn_net:
    """ Object holding all modules of the neural net

    Here, the all passes and weight updates are performed in consecutive fashion    
    """
    def __init__(self):
        """ Initialize empty net
        :paramters: 
        :return:
        """
        self.nn = []
        self.loss = []
        self.dropoutProbs = None
        self.epochs = []
        self.momentum = []
        self.lrs = []
        self.data_mean = None
        self.data_variance = None

    def new_net(self, dims, dropoutProbs = None):
        """ Create new net from given dimension. Optionally dropout rates can be passed

        The net will consist of hidden layers using ReLU and the output layer using
        softmax. The loss is calculated as the cross entropy error

        :parameters:
                dims: Dimensions of each layer, Nx1, N = # of layers
                dropoutProbs: dropout probabilities, (N-1)x1
        :return:
        """

        # Set dropout probabilities
        self.nn = []
        if dropoutProbs == None:
            dropoutProbs = [0 for i in range(len(dims) - 1)]
        self.dropoutProbs = dropoutProbs
        
        # Create weight and ReLu layers for input & hidden layers
        for i in range(0, len(dims) - 2):
            module = nn_modules.linear(dims[i], dims[i+1])
            module.init_weights(np.sqrt(4 / (dims[i] + dims[i+1])))
            self.nn.append(module)
            self.nn.append(nn_modules.relu(dims[i+1], dropoutProbs[i]))
        
        # Create softmax and cross entropy modules
        module = nn_modules.linear(dims[len(dims) - 2], dims[len(dims) - 1])
        module.init_weights(np.sqrt(2 / (dims[len(dims) - 2] + dims[len(dims) - 1])))
        self.nn.append(module)
        self.nn.append(nn_modules.softmax(dims[len(dims) - 1]))
        self.nn.append(nn_modules.crossEntropyLoss(dims[len(dims) - 1]))

    def fprop(self, X):
        """ Forward pass through the network with computation of loss
        
        The target must be set before calling this function.

        :paramters: X: Input data, NxD, D = dimension of input layer, N = # of data
        :return: loss
        """
        for module in self.nn:
            X = module.fprop(X)
        return np.mean(X)
    
    def test(self, X):
        """ Forward pass through the network without storing intermediate data but with
            computation of loss. Intended for testing the net.

            The target must be set before calling this function.

        :paramters: X: Input data, NxD, D = dimension of input layer, N = # of data
        :return: loss
        """
        for module in self.nn:
            X = module.test(X)
        return np.mean(X)
    
    def use(self, X):
        """ Forward pass through the network without storing intermediate data and 
            without loss computation.

        :paramters: X: Input data, NxD, D = dimension of input layer, N = # of data
        :return: output of softmax layer NxD, D = dimension of output layer, N = # of data
        """
        for module in self.nn:
            if module.__class__ != nn_modules.crossEntropyLoss:
                X = module.test(X)
        return X
    
    def bprop(self, errGradient):
        """ Backwards propagation of the error gradient

        :parameters: errGradient: error gradient
        :return:
        """
        for module in reversed(self.nn):
            errGradient = module.bprop(errGradient)
        
    def set_target(self, target):
        """ Set target for loss computation.

            This needs to be done before fprop() and test().

        :parameters: target: target of the batch, NxD, D = dimension of output layer, N = # of data
        :return:
        """
        self.nn[len(self.nn) - 1].set_target(target)
    
    def update_weights(self, learningfunction, params):
        """ Update the weights

            Is called after backprop().

        :paramters: learningfunction: function pointer to learningfunction
                    params: parameters for the learningfunction. Check learning_functions for further details
        :return:
        """
        for module in self.nn:
            if module.__class__ == nn_modules.linear:
                module.update_weights(learningfunction, params)

    def dropout(self):
        """ Apply dropout probabilities to the activation modules.

            Note: currently only ReLU functions apply the dropout!

        :parameters: 
        :return:
        """
        for module in self.nn:
            if module.__class__ == nn_modules.relu:
                module.dropout()

    def set_dropout(self, dropoutProbs):
        """ Set the dropout probabilies for the layers
            
            Note: Not sure if dropout works properly
        
        :parameters: dropoutprobs: dropout probabilities, (N-1)x1, N = # of layers
        :return:
        """
        self.dropoutProbs = dropoutProbs
        i = 0
        for module in self.nn:
            if module.__class__ == nn_modules.relu:
                module.dropoutProb = self.dropoutProbs[i]
                i += 1

    def save_to_disk(self, path):
        """ Saves the disk to a file, containing all information about this module
            including: dimensions, weights, bias, losses, dropoutrates, learningrates, momentum,...

        :paramters: path: path to file, including file name
        :return: 
        """
        diskNet = open(path, 'w')
        if len(self.epochs) > 0:
            saveNet = 'e'
            for t in self.epochs:
                saveNet += str(t) + ','
            saveNet = saveNet[:-1] + '\n'
            diskNet.write(saveNet)

        if self.data_mean != None and self.data_variance != None:
            saveNet = 'v' + str(self.data_mean) + ',' + str(self.data_variance)
            diskNet.write(saveNet+ '\n')

        if len(self.loss) > 0:
            saveNet = 'l'
            for t in self.loss:
                saveNet += str(t) + ','
            saveNet = saveNet[:-1] + '\n'
            diskNet.write(saveNet)

        if len(self.lrs) > 0:
            saveNet = 'r'
            for t in self.lrs:
                saveNet += str(t) + ','
            saveNet = saveNet[:-1] + '\n'
            diskNet.write(saveNet)

        if len(self.momentum) > 0:
            saveNet = 'm'
            for t in self.momentum:
                saveNet += str(t) + ','
            saveNet = saveNet[:-1] + '\n'
            diskNet.write(saveNet)

        if self.dropoutProbs != None:
            saveNet = 'd'
            for i in self.dropoutProbs:
                saveNet += str(i) + ','
            saveNet = saveNet[:-1] + '\n'
            diskNet.write(saveNet)

        for module in self.nn:
            diskNet.write(module.write_to_disk())
        diskNet.close

    def read_from_disk(self, path):
        """ Read a net from disk. All relevant information is taken from the file

            The read net is stored in this object.

        :paramters: path: path to file containing the net
        :return:
        """
        self.nn = []
        self.dropoutProbs = None
        self.loss = []
        diskNet = open(path, 'r')
        for line in diskNet:
            line = line[:-1]
            if line[0] == 'l':
                self.loss = []
                for l in line[1:].split(','):
                    self.loss.append(float(l))

            if line[0] == 'v':
                self.data_mean, self.data_variance = line[1:].split(',')

            if line[0] == 'e':
                self.epochs = []
                for l in line[1:].split(','):
                    self.epochs.append(int(l))

            if line[0] == 'r':
                self.lrs = []
                for l in line[1:].split(','):
                    self.lrs.append(float(l))
            
            if line[0] == 'm':
                self.momentum = []
                for l in line[1:].split(','):
                    self.momentum.append(float(l))

            if line[0] == 'd':
                self.dropoutProbs = []
                for l in line[1:].split(','):
                    self.dropoutProbs.append(float(l))
            
            elif line[0] == '0':
                dims, biasStr, weightsStr, dBStr, dWStr = line[2:].split('+')
                # Dimensions
                dimIn, dimOut = [int(x) for x in dims.split(',')]

                # Bias
                bias = []
                for b in biasStr.split(','):
                    bias.append(float(b))
                bias = np.array([bias], dtype = float)

                # Weights
                weights = []
                for wl in weightsStr.split(';'):
                    weightline = []
                    for w in wl.split(','):
                        weightline.append(float(w))
                    weights.append(weightline)
                weights = np.array(weights, dtype = float)

                # Bias gradient
                dB = []
                for b in dBStr.split(','):
                    dB.append(float(b))
                dB = np.array([dB], dtype = float)

                # Weight gradient
                dW = []
                for wl in dWStr.split(';'):
                    weightline = []
                    for w in wl.split(','):
                        weightline.append(float(w))
                    dW.append(weightline)
                dW = np.array(dW, dtype = float)

                # Create module
                module = nn_modules.linear(dimIn, dimOut)
                module.weights = weights
                module.bias = bias
                module.dW = dW
                module.dB = dB
                self.nn.append(module)
            
            elif line[0] == '1':
                dim, dropout = line[2:].split('+')
                self.nn.append(nn_modules.relu(int(dim), float(dropout)))

            elif line[0] == '2':
                dim = int(line[2:])
                self.nn.append(nn_modules.softmax(dim))
            elif line[0] == '3':
                dim = int(line[2:])
                self.nn.append(nn_modules.crossEntropyLoss(dim))
        diskNet.close
    