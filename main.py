import numpy as np
import nn_modules
from read_input import read_dataset, normalize_data
import learning_functions
from functools import partial
from nn_net import nn_net
import matplotlib.pyplot as plt

pData = 'data/trainset.txt'

path = 'savedNets/'

#network dimension
netDim = [64, 512, 64]

# dropout for each of the hidden layers (for now always set to 0)
dropout = [0]

# create empty net
nn = nn_net()

# create new net
nn.new_net(netDim, dropout)

# or load from disk
#nn.read_from_disk(path + 'net_name')

# load training data
data, label = read_dataset(pData)

# normalize the data by computing the mean and variance
# and save them for application of the net
data, nn.data_mean, nn.data_variance = normalize_data(data)

# or normalize with mean and variance read from disk
#data,_,_ = normalize_data(data, nn.data_mean, nn.data_variance)

# set parameters for learning function (look into "learning_functions.py" to see what you need)
lr = 0.3
momentum = 0.9
learningParams = [momentum, lr]
# set learning function
learningFunc = partial(learning_functions.momentum)

# define amount of epochs and batch size
epochs = 200
batchSize = 600

# compute how many batches are needed
N = data.shape[0]
batchNum = int(N / batchSize)

# iterate over epochs
for i in range(epochs):
    # determine random choice from training data, to be assinged to each batch
    indices = np.random.permutation(np.arange(N))
    batchLoss = []
    #iterate over batches
    for j in range(batchNum):
        # apply dropout (e.g. determine different nodes to drop out)
        nn.dropout()

        # apply batch assignment
        batch_indices = indices[np.arange(j * batchSize, (j + 1) * batchSize)]
        x = np.array(data[batch_indices, :])
        y = np.array(label[batch_indices])

        # set target (needs to be done BEFORE fprop, loss is calculated with the target)
        nn.set_target(y)

        # forward propagation
        l = nn.fprop(x)

        # for later visualization
        batchLoss.append(l)

        # compute error gradient for backpropagation
        errGradient = np.tile(1.0 / batchSize, (batchSize, 1))

        # backpropagate and update the weights
        nn.bprop(errGradient)
        nn.update_weights(learningFunc, learningParams)
    
    # loss for the epoch and print it
    l = np.mean(np.array(batchLoss))
    nn.loss.append(l)
    print(i,l)

# to save the neural net to disk, a name is derived from the structure, the amount of epochs and the learning rate & momentum
nn.epochs.append(epochs)
nn.lrs.append(lr)
nn.momentum.append(momentum)
netEp = sum(nn.epochs)

# save the net to disk
nn.save_to_disk(path + ''.join(str(x)+'x' for x in netDim)[:-1] + '_ep' + str(netEp) + '.txt')