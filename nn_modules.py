import abc
import numpy as np
import learning_functions

class nnModule:
    """ Abstract class defining the interface of each neural network module
    """
    __metaclass__ = abc.ABCMeta
    @abc.abstractmethod
    def fprop(self, X):
        """ Forward pass through the module

        :parameters: X: Input tensor for the module, numpy array, N x D
        :return: output tensor after module application, numpy array, N x D
                 or loss for error modules (float)
        """
        return

    @abc.abstractmethod
    def bprop(self, errGradient):
        """ Backpropagation of the error gradient through the module
            
        :parameters: errGradient: Error gradient of the previous module
        :return: Error gradient of this module
        """
        return

    @abc.abstractmethod
    def test(self, X):
        """ Forward pass through the module without storing activations 
            for backpropagation

        :parameters: X: Input tensor for the module, numpy array, N x D
        :return: output tensor after module application, numpy array, N x D
                 or loss for error modules (float)
        """
        return
    
    @abc.abstractmethod
    def write_to_disk(self):
        """ Derive a string containing information about the module like weights, bias, 
            dropout rate etc.

        :parameter:
        :return: String containing information about module
        """
        return

class nnActModule(nnModule):
    """ Specialization for non-linear modules without parameters
        i.e. layers applying an activation function
    """
    __metaclass__ = abc.ABCMeta

class nnLossModule(nnModule):
    """ Specialization for loss modules, no parameters
    """
    __metaclass__ = abc.ABCMeta

    def set_target(self, target):
        self.target = target

class linear(nnModule):
    def __init__(self, dimIn, dimOut):
        self.weights = None
        self.dimIn = dimIn
        self.dimOut = dimOut
        self.bias = None
        self.X = None
        self.dE = None
        self.dW = np.zeros((dimIn, dimOut))
        self.dB = np.zeros((1, dimOut))

    def init_weights(self, sigma):
        self.weights = np.random.normal(0, sigma, (self.dimIn, self.dimOut))
        self.bias = np.zeros((1, self.dimOut))
        
    def fprop(self, X):
        self.X = X
        return self.X @ self.weights + self.bias
    
    def test(self, X):
        return self.fprop(X)

    def bprop(self, errGrad):
        self.dE = errGrad
        return errGrad @ self.weights.T

    def update_weights(self, learningfunction, params):
        self.dW, self.dB = learningfunction(self.dE, self.dW, self.dB, self.X, params)
        self.weights -= self.dW
        self.bias -= self.dB
    
    def write_to_disk(self):
        """ Saves module in the following fashion:
        
        0+dimIn,dimOut+bias+weights+dB+dW
        biases are separated by ','
        weights are separated by ',' and lines in the matrix by ';'
        """
        saveString = '0+' + str(self.dimIn) + ',' + str(self.dimOut) + '+'
        for i in self.bias[0]:
            saveString += str(i) + ','
        saveString = saveString[:-1] + '+'
        for i in self.weights:
            for j in i:
                saveString += str(j) + ','
            saveString = saveString[:-1] + ';'
        saveString = saveString[:-1] + '+'

        for i in self.dB[0]:
            saveString += str(i) + ','
        saveString = saveString[:-1] + '+'
        for i in self.dW:
            for j in i:
                saveString += str(j) + ','
            saveString = saveString[:-1] + ';'
        saveString = saveString[:-1] + '\n'

        return saveString

class relu(nnActModule):
    def __init__(self, dim, dropoutProb = 0):
        self.dim = dim
        self.dropoutProb = dropoutProb
        self.Y = None

    def fprop(self, X):
        X[X < 0] = 0
        self.Y = X
        return self.Y * self.dropOut

    def dropout(self, dropoutProb = None):
        if dropoutProb != None:
            self.dropoutProb = dropoutProb
        self.dropOut = np.random.choice([0,1], (1, self.dim), p = [self.dropoutProb, 1-self.dropoutProb])

    def test(self, X):
        X[X < 0] = 0
        self.Y = X
        return self.Y * (1 - self.dropoutProb)

    def bprop(self, errGrad):
        batchSize = errGrad.shape[0]
        dE = np.zeros((batchSize, self.dim))
        z = np.zeros((batchSize, self.dim))
        dE[self.Y > 0] = 1
        for i in range(batchSize):
            z[i, :] = np.dot(errGrad[i,:], dE[i, :])
        return z

    def write_to_disk(self):
        saveString = '1+' + str(self.dim) + '+' + str(self.dropoutProb) + '\n'
        return saveString

class softmax(nnActModule):
    def __init__(self, dim):
        self.dim = dim
        self.Y = None

    def fprop(self, X):
        x_max = np.max(X, 1)
        exp = np.exp((X.T - x_max).T)
        self.Y = (exp.T / np.sum(exp, 1)).T
        return self.Y

    def test(self, X):
        return self.fprop(X)

    def bprop(self, errGrad):
        batchSize = errGrad.shape[0]
        v = np.empty((batchSize, 1))
        for i in range(batchSize):
            v[i, :] = np.dot(errGrad[i, :], self.Y[i, :])
        v = errGrad - np.broadcast_to(v, (batchSize, self.dim))
        return self.Y * v
    
    def write_to_disk(self):
        saveString = '2+' + str(self.dim) + '\n'
        return saveString
        
class crossEntropyLoss(nnLossModule):
    def __init__(self, dim):
        self.X = None
        self.dim = dim
        self.target = None

    def test(self, X):
        return self.fprop(X)

    def fprop(self, X):
        self.X = X
        loss = -1 * np.log(X[np.arange(X.shape[0]), self.target])
        return loss
    
    def bprop(self, errGradient):
        batchSize = errGradient.shape[0]
        z = np.zeros((batchSize, self.dim))
        z[np.arange(batchSize), self.target] = -1 * 1.0/self.X[np.arange(batchSize), self.target]
        np.multiply(errGradient, z, z)
        return z

    def write_to_disk(self):
        saveString = '3+' + str(self.dim) + '\n'
        return saveString